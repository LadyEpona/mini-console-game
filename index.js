#!/usr/bin/env node

import chalk from "chalk";
import inquirer from "inquirer";
import gradient from "gradient-string";
import chalkAnimation from "chalk-animation";
import figlet from "figlet";
import { createSpinner } from "nanospinner";

let playerName;

const sleep = (ms = 2000) => new Promise((r) => setTimeout(r, ms));

async function welcome() {
  const rainbowTitle = chalkAnimation.rainbow(
    "It's A Teenie Weenie JavaScript Quiz \n"
  );

  await sleep();
  rainbowTitle.stop();

  console.log(`
        ${chalk.bgBlue("HOW TO PLAY")}
        I am a process on your computer.
        If you get any question wrong I will be ${chalk.bgRed("killed")}
        So get all the questions right...
    `);
}

async function askName() {
  const answer = await inquirer.prompt({
    name: "player_name",
    type: "input",
    message: "What is your name?",
    default() {
      return "Player";
    },
  });

  playerName = answer.player_name;
}

async function handleAnswer(isCorrect) {
  const spinner = createSpinner("Checking answer...").start();
  await sleep();

  if (isCorrect) {
    spinner.success({
      text: `🎉 ${chalk.green(`Nice work ${playerName}. Keep it up!`)} 🎉`,
    });
  } else {
    spinner.error({
      text: `💀 ${chalk.red(`Game over, you lose ${playerName}!`)} 💀`,
    });
    process.exit(1);
  }
}

function winner() {
  console.clear();
  const msg = `Congratulations, ${playerName} ! \n You won!`;

  figlet(msg, (err, data) => {
    console.log(gradient.pastel.multiline(data));
  });
}

async function question1() {
  const answer = await inquirer.prompt({
    name: "question_1",
    type: "list",
    message: "Which one of these array methods mutates the original array? ",
    choices: ["Map", "Reverse", "Filter", "Reduce"],
  });

  return handleAnswer(answer.question_1 === "Reverse");
}

async function question2() {
  const answer = await inquirer.prompt({
    name: "question_2",
    type: "list",
    message: "Which of the following is NOT a looping structure in JavaScript?",
    choices: ["For loops", "While loops", "Do-While loops", "Do-During loops"],
  });

  return handleAnswer(answer.question_2 === "Do-During loops");
}

async function question3() {
  const answer = await inquirer.prompt({
    name: "question_3",
    type: "input",
    message: "What would be the result of 3 + 2 + '7' + 1_1?",
  });

  return handleAnswer(answer.question_3 === "5711");
}

async function question4() {
  const answer = await inquirer.prompt({
    name: "question_4",
    type: "checkbox",
    message: "What are all the types of Pop up boxes available in JavaScript?",
    choices: ["Alert", "Pop", "Note", "Tip", "Confirm", "Prompt", "Error"],
  });

  return handleAnswer(
    JSON.stringify(answer.question_4) ===
      JSON.stringify(["Alert", "Confirm", "Prompt"])
  );
}

async function question5() {
  const answer = await inquirer.prompt({
    name: "question_5",
    type: "confirm",
    message: "Can function parameters have default values?",
  });

  return handleAnswer(answer.question_5 === true);
}

await welcome();
await askName();
await question1();
await question2();
await question3();
await question4();
await question5();
await winner();
